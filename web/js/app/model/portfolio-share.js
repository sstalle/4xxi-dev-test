define(['Backbone'], function (Backbone) {
    return Backbone.Model.extend({
        defaults: {
            code: '',
            count: 1
        }
    });
});