define(['Backbone'], function (Backbone) {
    return Backbone.Model.extend({
        defaults: {
            code: null
        }
    });
});