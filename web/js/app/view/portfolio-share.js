define(['jquery', 'Backbone'], function ($, Backbone) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'form-group',

        events: {
            'click .js-delete-share': 'delete'
        },

        initialize: function(options) {
            this.templateHtml = $('.js-portfolio-shares').data('prototype');
            this.shareIndex = options.shareIndex || 0;
        },

        render: function () {
            var template = $(this.templateHtml.replace(/__name__/g, this.shareIndex)),
                shareCodeElements = template.find('.js-share-code'),
                model = this.model;

            shareCodeElements.filter('input').each(function() {
                $(this).val(model.get('code'));
            });
            shareCodeElements.filter('span').each(function () {
                $(this).text(model.get('code'));
            });
            template.find('.js-share-count').val(model.get('count'));

            this.$el.html(template);

            return this;
        },

        delete: function (e) {
            e.preventDefault();
            this.model.destroy();
            this.remove();
        }
    });
});