define(['Backbone', 'view/portfolio-share'], function (Backbone, PortfolioShareView) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'js-portfolio-form',

        initialize: function () {
            this.nextShareIndex = this.collection.length;
            this.listenTo(this.collection, 'add', this.add);

            this.collection.each(function (Share) {
                new PortfolioShareView({
                    el: this.$('[data-share-code="' + Share.get('code') + '"]'),
                    model: Share
                })
            }.bind(this));
        },

        add: function (PortfolioShare) {
            var shareView = new PortfolioShareView({model: PortfolioShare, shareIndex: this.nextShareIndex++});
            this.$el.append(shareView.render().el);
        }
    });
});