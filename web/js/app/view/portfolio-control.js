define(['jquery', 'Backbone'], function ($, Backbone) {
    return Backbone.View.extend({
        tagName: 'div',
        template: _.template($('.js-share-adder-template').first().html()),

        events: {
            'click .js-add-share': 'addShare',
            'keyup .js-share-count': 'updateCount'
        },

        initialize: function (options) {
            this.shareCollection = options.shareCollection;
            this.portfolioCollection = options.portfolioCollection;
            this.listenTo(this.shareCollection, 'update reset', this.render);
        },

        render: function () {
            var shareCodes = this.shareCollection.map(function (Share) {
                return Share.get('code');
            });

            this.$el.html(this.template({shareCodes: shareCodes, model: this.model.attributes}));
            this.$('.js-adder-control').toggleClass('invisible', !shareCodes.length);
            this.$('.js-submit-control').toggleClass('invisible', !this.portfolioCollection.length)

            return this;
        },

        updateCount: function (e) {
            this.model.set('count', $(e.target).val())
        },

        addShare: function () {
            var currentCount = this.$('.js-share-count').val();
            this.model.set('count', 1);

            this.portfolioCollection.add({
                code: this.$('.js-share-code').val(),
                count: currentCount
            });
        }
    });
});