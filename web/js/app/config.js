require.config({
    baseUrl: '/js/app',
    paths: {
        jquery: '../jquery.min',
        underscore: '../underscore-min',
        Backbone: '../backbone-min'
    }
});