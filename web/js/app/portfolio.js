define(
    [
        'jquery',
        'collection/portfolio-share',
        'collection/share',
        'model/portfolio-share',
        'view/portfolio-control',
        'view/portfolio'
    ],
    function ($,
              PortfolioShareCollection,
              ShareCollection,
              PortfolioShare,
              PortfolioControlView,
              PortfolioView) {
        return {
            init: function (portfolioShares, allShares) {
                $(document).ready(function () {
                    var portfolioShareCollection = new PortfolioShareCollection(portfolioShares),
                        shareCollection = new ShareCollection(allShares, {portfolioShareCollection: portfolioShareCollection}),
                        shareAdderView = new PortfolioControlView({
                            model: new PortfolioShare(),
                            shareCollection: shareCollection,
                            portfolioCollection: portfolioShareCollection
                        });


                    new PortfolioView({el: $('.js-portfolio-shares').first(), collection: portfolioShareCollection});

                    $('.js-share-adder').append(shareAdderView.render().$el);
                });
            }
        };
    }
);