define(['Backbone', 'model/portfolio-share'], function (Backbone, PortfolioShare) {
    return Backbone.Collection.extend({
        model: PortfolioShare,

        getShareCodes: function () {
            return this.map(function (PortfolioShare) {
                return PortfolioShare.get('code');
            })
        }
    });
});
