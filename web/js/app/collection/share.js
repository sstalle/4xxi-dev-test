define(['underscore', 'Backbone', 'model/share'], function (_, Backbone, Share) {
    return Backbone.Collection.extend({
        model: Share,

        comparator: function (Share) {
            return Share.get('code');
        },

        constructor: function (allShares, options) {
            this.allShares = allShares;
            this.portfolioShareCollection = options.portfolioShareCollection;
            this.listenTo(this.portfolioShareCollection, 'update reset', this.update);
            Backbone.Collection.apply(this, arguments);
            this.update();
        },

        update: function () {
            this.reset(this.getAvailableShares());
        },

        getAvailableShares: function () {
            var selectedShareCodes = this.portfolioShareCollection.getShareCodes();

            return _.filter(this.allShares, function (shareObject) {
                return !_.contains(selectedShareCodes, shareObject.code);
            });
        }
    });
});