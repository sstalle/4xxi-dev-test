<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150925170516 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_portfolio_share (id INT AUTO_INCREMENT NOT NULL, portfolio_id INT NOT NULL, share_code VARCHAR(50) NOT NULL, INDEX IDX_E8849296B96B5643 (portfolio_id), INDEX IDX_E8849296CAD23D01 (share_code), UNIQUE INDEX portfolio_share_unique_idx (portfolio_id, share_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_share (code VARCHAR(50) NOT NULL, PRIMARY KEY(code)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_portfolio (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, UNIQUE INDEX UNIQ_1824F57A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_portfolio_share ADD CONSTRAINT FK_E8849296B96B5643 FOREIGN KEY (portfolio_id) REFERENCES app_portfolio (id)');
        $this->addSql('ALTER TABLE app_portfolio_share ADD CONSTRAINT FK_E8849296CAD23D01 FOREIGN KEY (share_code) REFERENCES app_share (code)');
        $this->addSql('ALTER TABLE app_portfolio ADD CONSTRAINT FK_1824F57A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_portfolio_share DROP FOREIGN KEY FK_E8849296CAD23D01');
        $this->addSql('ALTER TABLE app_portfolio_share DROP FOREIGN KEY FK_E8849296B96B5643');
        $this->addSql('DROP TABLE app_portfolio_share');
        $this->addSql('DROP TABLE app_share');
        $this->addSql('DROP TABLE app_portfolio');
    }
}
