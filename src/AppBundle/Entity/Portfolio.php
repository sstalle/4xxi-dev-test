<?php

namespace AppBundle\Entity;

use AppBundle\Exception\DuplicateShareException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_portfolio")
 */
class Portfolio implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="portfolio")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     *
     * @var User
     */
    protected $user;

    /**
     * @Assert\Count(min=1, groups={"portfolio"})
     *
     * @ORM\OneToMany(targetEntity="PortfolioShare", mappedBy="portfolio", orphanRemoval=true, cascade={"all"})
     *
     * @var PortfolioShare[]
     */
    protected $shares;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $user->setPortfolio($this);

        $this->shares = new ArrayCollection();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function addShare(PortfolioShare $share)
    {
        $existingShares = $this->shares->filter(
            function (PortfolioShare $existingShare) use ($share) {
                return $existingShare->getShareCode() === $share->getShareCode();
            }
        );

        if (count($existingShares)) {
            throw new DuplicateShareException(
                sprintf(
                    'Share \'%s\' already exists in portfolio',
                    $share->getShareCode()
                )
            );
        }

        $this->shares->add($share);
        $share->setPortfolio($this);
    }

    public function removeShare(PortfolioShare $share)
    {
        $this->shares->removeElement($share);
        $share->setPortfolio(null);
    }

    /**
     * @return PortfolioShare[]
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * @return bool
     */
    public function hasShares()
    {
        return count($this->shares) > 0;
    }

    public function jsonSerialize()
    {
        $data = [];

        foreach ($this->getShares() as $share) {
            $data[] = $share->jsonSerialize();
        }

        return $data;
    }
}
