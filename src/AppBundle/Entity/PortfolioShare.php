<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *  name="app_portfolio_share",
 *  uniqueConstraints={@ORM\UniqueConstraint(name="portfolio_share_unique_idx", columns={"portfolio_id", "share_code"})}
 * )
 */
class PortfolioShare implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Portfolio", inversedBy="shares")
     * @ORM\JoinColumn(name="portfolio_id", nullable=false)
     *
     * @var Portfolio
     */
    protected $portfolio;

    /**
     * @Assert\NotNull(groups={"portfolio"})
     *
     * @ORM\ManyToOne(targetEntity="Share")
     * @ORM\JoinColumn(name="share_code", referencedColumnName="code", nullable=false)
     *
     * @var Share
     */
    protected $share;

    /**
     * @Assert\NotNull(groups={"portfolio"})
     * @Assert\GreaterThanOrEqual(value=1, groups={"portfolio"})
     *
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    protected $count = 1;

    /**
     * @return Portfolio
     */
    public function getPortfolio()
    {
        return $this->portfolio;
    }

    /**
     * @param Portfolio $portfolio
     */
    public function setPortfolio(Portfolio $portfolio = null)
    {
        $this->portfolio = $portfolio;
    }

    /**
     * @return Share
     */
    public function getShare()
    {
        return $this->share;
    }

    /**
     * @return string
     */
    public function getShareCode()
    {
        return $this->share->getCode();
    }

    /**
     * @param Share $share
     */
    public function setShare(Share $share)
    {
        $this->share = $share;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    public function jsonSerialize()
    {
        return [
            'code' => $this->getShareCode(),
            'count' => $this->getCount(),
        ];
    }
}
