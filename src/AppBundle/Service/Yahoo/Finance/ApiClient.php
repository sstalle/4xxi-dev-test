<?php

namespace AppBundle\Service\Yahoo\Finance;

use AppBundle\Exception\Yahoo\Finance\RequestFailureException;
use AppBundle\Service\Yahoo\Finance\Query\HistoricalDataQuery;
use AppBundle\Service\Yahoo\Finance\Result\HistoricalData;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

class ApiClient implements ApiClientInterface
{
    const BASE_URL = 'https://query.yahooapis.com/v1/public/yql?';

    /**
     * @var ClientInterface
     */
    protected $httpClient;

    /**
     * @var JsonResponseDataConverter
     */
    protected $dataConverter;

    /**
     * @param ClientInterface                $client
     * @param ResponseDataConverterInterface $dataConverter
     */
    public function __construct(ClientInterface $client, ResponseDataConverterInterface $dataConverter)
    {
        $this->httpClient = $client;
        $this->dataConverter = $dataConverter;
    }

    /**
     * @param HistoricalDataQuery $query
     *
     * @return HistoricalData[]
     *
     * @throws RequestFailureException
     */
    public function getHistoricalData(HistoricalDataQuery $query)
    {
        if ($query->getTo() < $query->getFrom()) {
            throw new \InvalidArgumentException(
                sprintf(
                    'To date %s is less than from date %s',
                    $query->getTo()->format('Y-m-d'),
                    $query->getFrom()->format('Y-m-d')
                )
            );
        }

        if (!count($query->getSymbols())) {
            throw new \InvalidArgumentException('No symbols specified');
        }

        // Yahoo API fails with "Too many instructions executed" error for large result sets,
        // so we have to make a lot of requests...
        $results = [];

        $queryDates = [];
        foreach (new \DatePeriod($query->getFrom(), new \DateInterval('P1Y'), $query->getTo()) as $periodDate) {
            $queryDates[] = $periodDate;
        }

        $lastQueryDate = end($queryDates);
        if ($lastQueryDate < $query->getTo()) {
            $queryDates[] = $query->getTo();
        }

        foreach ($query->getSymbols() as $symbol) {
            $symbolQueryDates = $queryDates;
            /** @var \DateTime $previousQueryDate */
            $previousQueryDate = array_shift($symbolQueryDates);

            /** @var \DateTime $nextQueryDate */
            foreach ($symbolQueryDates as $nextQueryDate) {
                $response = $this->executeQuery(
                    sprintf(
                        'select *
                          from yahoo.finance.historicaldata
                          where symbol = "%s" and startDate = "%s" and endDate = "%s"
                        ',
                        $symbol,
                        $previousQueryDate->format('Y-m-d'),
                        $nextQueryDate->format('Y-m-d')
                    )
                );

                $results = array_merge($results, $this->dataConverter->convertHistoricalDataResponse($response));
                $previousQueryDate = $nextQueryDate;
            }
        }

        return $results;
    }

    /**
     * @param string $query
     *
     * @return \Psr\Http\Message\ResponseInterface
     *
     * @throws RequestFailureException
     */
    protected function executeQuery($query)
    {
        $commonRequestParameters = [
            'env' => 'http://datatables.org/alltables.env',
            'format' => 'json',
            'q' => $query,
        ];

        try {
            $response = $this->httpClient->request(
                'GET',
                static::BASE_URL.http_build_query($commonRequestParameters)
            );
        } catch (RequestException $e) {
            throw new RequestFailureException('Error executing request to Yahoo API', 0, $e);
        }

        if ($response->getStatusCode() != 200) {
            throw new RequestFailureException(sprintf('Got status code %d from Yahoo API', $response->getStatusCode()));
        }

        return $response;
    }
}
