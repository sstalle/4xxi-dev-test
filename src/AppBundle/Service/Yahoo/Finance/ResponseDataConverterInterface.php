<?php

namespace AppBundle\Service\Yahoo\Finance;

use AppBundle\Exception\Yahoo\Finance\ConversionFailureException;
use Psr\Http\Message\ResponseInterface;

interface ResponseDataConverterInterface
{
    /**
     * @param ResponseInterface $response
     *
     * @return Result\HistoricalData[]
     *
     * @throws ConversionFailureException
     */
    public function convertHistoricalDataResponse(ResponseInterface $response);
}
