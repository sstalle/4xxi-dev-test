<?php

namespace AppBundle\Service\Yahoo\Finance;

use AppBundle\Exception\Yahoo\Finance\ConversionFailureException;
use AppBundle\Service\Yahoo\Finance\Result\HistoricalData;
use Psr\Http\Message\ResponseInterface;

class JsonResponseDataConverter implements ResponseDataConverterInterface
{
    /**
     * @param ResponseInterface $response
     *
     * @return Result\HistoricalData[]
     *
     * @throws ConversionFailureException
     */
    public function convertHistoricalDataResponse(ResponseInterface $response)
    {
        $decodedBody = json_decode($response->getBody(), true);
        if ($decodedBody === null) {
            throw new ConversionFailureException('Request body could not be decoded');
        }

        $results = [];
        if (!isset($decodedBody['query']['results']['quote'])
            || !($quotes = $decodedBody['query']['results']['quote'])
        ) {
            return $results;
        }

        foreach ($quotes as $quote) {
            $date = \DateTime::createFromFormat('Y-m-d', $quote['Date']);
            $date->setTime(0, 0, 0);

            $results[] = new HistoricalData(
                $quote['Symbol'],
                $date,
                $quote['Close'],
                $quote['Adj_Close']
            );
        }

        return $results;
    }
}
