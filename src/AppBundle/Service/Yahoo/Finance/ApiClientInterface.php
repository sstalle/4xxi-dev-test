<?php

namespace AppBundle\Service\Yahoo\Finance;

use AppBundle\Exception\Yahoo\Finance\RequestFailureException;
use AppBundle\Service\Yahoo\Finance\Query\HistoricalDataQuery;
use AppBundle\Service\Yahoo\Finance\Result\HistoricalData;

interface ApiClientInterface
{
    /**
     * @param HistoricalDataQuery $query
     *
     * @return HistoricalData[]
     *
     * @throws RequestFailureException
     */
    public function getHistoricalData(HistoricalDataQuery $query);
}
