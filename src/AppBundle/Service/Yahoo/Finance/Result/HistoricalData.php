<?php

namespace AppBundle\Service\Yahoo\Finance\Result;

class HistoricalData
{
    /**
     * @var string
     */
    protected $symbol;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var string
     */
    protected $close;

    /**
     * @var string
     */
    protected $adjustedClose;

    /**
     * @param string    $symbol
     * @param \DateTime $date
     * @param string    $close
     * @param string    $adjustedClose
     */
    public function __construct($symbol, \DateTime $date, $close, $adjustedClose)
    {
        $this->symbol = $symbol;
        $this->date = $date;
        $this->close = $close;
        $this->adjustedClose = $adjustedClose;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getClose()
    {
        return $this->close;
    }

    /**
     * @return string
     */
    public function getAdjustedClose()
    {
        return $this->adjustedClose;
    }
}
