<?php

namespace AppBundle\Service\Yahoo\Finance\Query;

use Doctrine\Common\Collections\ArrayCollection;

class HistoricalDataQuery
{
    /**
     * @var \DateTime
     */
    protected $from;

    /**
     * @var \DateTime
     */
    protected $to;

    /**
     * @var string[]
     */
    protected $symbols;

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param \string[] $symbols
     */
    public function __construct(\DateTime $from, \DateTime $to, array $symbols = [])
    {
        $this->from = $from;
        $this->to = $to;
        $this->symbols = new ArrayCollection($symbols);
    }

    /**
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return \string[]
     */
    public function getSymbols()
    {
        return $this->symbols;
    }

    /**
     * @param string $symbol
     */
    public function addSymbol($symbol)
    {
        if (!$this->symbols->contains($symbol)) {
            $this->symbols->add($symbol);
        }
    }

    /**
     * @param string $symbol
     */
    public function removeSymbol($symbol)
    {
        $this->symbols->removeElement($symbol);
    }
}
