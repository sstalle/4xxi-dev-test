<?php

namespace AppBundle\Service\Portfolio\Chart\Historical;

use AppBundle\Exception\Chart\DataSource\BadDataException;
use AppBundle\Service\Yahoo\Finance\ApiClientInterface;
use AppBundle\Service\Yahoo\Finance\Query\HistoricalDataQuery;
use AppBundle\Exception\Yahoo\Finance\Exception as ApiException;

class YahooFinanceChartDataSource implements ChartDataSourceInterface
{
    /**
     * @var ApiClientInterface
     */
    protected $apiClient;

    /**
     * @param ApiClientInterface $apiClient
     */
    public function __construct(ApiClientInterface $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function getData(ChartRequest $request)
    {
        $apiQuery = $this->convertToApiQuery($request);

        try {
            $historicalData = $this->apiClient->getHistoricalData($apiQuery);
        } catch (ApiException $e) {
            throw new BadDataException('Could not fetch data from remote API', 0, $e);
        }

        $shareCounts = [];
        foreach ($request->getShares() as $share) {
            $shareCounts[$share->getShareCode()] = $share->getCount();
        }

        $result = [];
        foreach ($historicalData as $dayData) {
            $timestamp = $dayData->getDate()->format('U');
            if (!isset($result[$timestamp])) {
                $result[$timestamp] = 0;
            }

            $result[$timestamp] += $dayData->getAdjustedClose() * $shareCounts[$dayData->getSymbol()];
        }

        return $result;
    }

    protected function convertToApiQuery(ChartRequest $request)
    {
        $symbols = [];
        foreach ($request->getShares() as $share) {
            $symbols[] = $share->getShareCode();
        }

        return new HistoricalDataQuery($request->getFrom(), $request->getTo(), $symbols);
    }
}
