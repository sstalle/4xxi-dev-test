<?php

namespace AppBundle\Service\Portfolio\Chart\Historical;

use AppBundle\Entity\PortfolioShare;

class ChartRequest
{
    /**
     * @var \DateTime
     */
    protected $from;

    /**
     * @var \DateTime
     */
    protected $to;

    /**
     * @var PortfolioShare[]
     */
    protected $shares;

    /**
     * @param \DateTime                          $from
     * @param \DateTime                          $to
     * @param \AppBundle\Entity\PortfolioShare[] $shares
     */
    public function __construct(\DateTime $from, \DateTime $to, array $shares = [])
    {
        $this->from = $from;
        $this->to = $to;
        $this->shares = $shares;
    }

    /**
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return \AppBundle\Entity\PortfolioShare[]
     */
    public function getShares()
    {
        return $this->shares;
    }
}
