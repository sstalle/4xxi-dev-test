<?php

namespace AppBundle\Service\Portfolio\Chart\Historical;

use AppBundle\Entity\Portfolio;
use AppBundle\Exception\Chart\DataSource\BadDataException;
use Ob\HighchartsBundle\Highcharts\Highstock;

class ChartPresenter
{
    /**
     * @var ChartFactory
     */
    protected $chartFactory;

    /**
     * @var ChartDataSourceInterface
     */
    protected $chartDataSource;

    /**
     * @var string|null
     */
    protected $error;

    /**
     * @var Highstock|null
     */
    protected $chart;

    /**
     * @param ChartFactory             $chartFactory
     * @param ChartDataSourceInterface $chartDataSource
     */
    public function __construct(ChartFactory $chartFactory, ChartDataSourceInterface $chartDataSource)
    {
        $this->chartFactory = $chartFactory;
        $this->chartDataSource = $chartDataSource;
    }

    public function init(\DateTime $from, \DateTime $to, Portfolio $portfolio = null)
    {
        if (!$portfolio || !$portfolio->hasShares()) {
            $this->error = 'Необходимо добавить акции в порфель';

            return;
        }

        $shares = [];
        foreach ($portfolio->getShares() as $share) {
            $shares[] = $share;
        }

        try {
            $chartData = $this->chartDataSource->getData(new ChartRequest($from, $to, $shares));
        } catch (BadDataException $e) {
            $this->error = 'Не удалось получить данные для графика';

            return;
        }

        if (!$chartData) {
            $this->error = 'Нет данных для графика за выбранный период';

            return;
        }

        $this->chart = $this->chartFactory->createChart($chartData);
    }

    /**
     * @return null|string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return null|Highstock
     */
    public function getChart()
    {
        return $this->chart;
    }
}
