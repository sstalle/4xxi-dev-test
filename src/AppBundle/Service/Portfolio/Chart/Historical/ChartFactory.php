<?php

namespace AppBundle\Service\Portfolio\Chart\Historical;

use Ob\HighchartsBundle\Highcharts\Highstock;

class ChartFactory
{
    /**
     * @param array $data Data returned from ChartDataSourceInterface's implementation
     *
     * @return Highstock
     */
    public function createChart(array $data)
    {
        if (!$data) {
            throw new \InvalidArgumentException('Cannot create chart with empty dataset');
        }

        $chart = new Highstock();
        $chart->series($this->buildSeries($data));
        $chart->title->text('Стоимость портфеля от времени');
        $chart->chart->renderTo('chart');

        return $chart;
    }

    protected function buildSeries(array $data)
    {
        ksort($data);

        $seriesData = [];
        foreach ($data as $timestamp => $portfolioValue) {
            $seriesData[] = [$timestamp * 1000, $portfolioValue];
        }

        return [
            [
                'name' => 'Стоимость',
                'data' => $seriesData,
                'tooltip' => [
                    'valueDecimals' => 2,
                ],
            ],
        ];
    }
}
