<?php

namespace AppBundle\Service\Portfolio\Chart\Historical;

interface ChartDataSourceInterface
{
    /**
     * @param ChartRequest $request
     *
     * @return [] Associative array: unix timestamps as keys, total portfolio values as values
     */
    public function getData(ChartRequest $request);
}
