<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Share;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadShareData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach (['RHT', 'MSFT', 'YHOO', 'AAPL', 'IBM', 'SGI'] as $code) {
            $share = new Share($code);
            $manager->persist($share);
        }

        $manager->flush();
    }
}
