<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Portfolio;
use AppBundle\Entity\Share;
use AppBundle\Entity\User;
use AppBundle\Form\Type\PortfolioType;
use AppBundle\Service\Portfolio\Chart\Historical\ChartPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_USER')")
 */
class PortfolioController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        if (!($portfolio = $user->getPortfolio())) {
            $portfolio = new Portfolio($user);
        }

        $currentPageUrl = $this->generateUrl('homepage');
        $portfolioEditForm = $this->createForm(
            PortfolioType::TYPE_NAME,
            $portfolio,
            [
                'action' => $currentPageUrl,
            ]
        );
        $portfolioEditForm->handleRequest($request);

        if ($portfolioEditForm->isValid()) {
            // Ugliest workaround for Doctrine's commit order bug
            $newShares = [];
            foreach ($portfolio->getShares() as $share) {
                $newShares[] = $share;
                $portfolio->removeShare($share);
            }

            $em->persist($portfolio);
            $em->flush();

            foreach ($newShares as $share) {
                $portfolio->addShare($share);
            }

            $em->flush();

            return $this->redirect($currentPageUrl);
        }

        $allAvailableShares = $em->getRepository(Share::class)->findAll();

        return $this->render(
            'portfolio/index.html.twig',
            [
                'form' => $portfolioEditForm->createView(),
                'all_shares' => $allAvailableShares,
            ]
        );
    }

    /**
     * @Route("/chart", name="chart")
     */
    public function chartAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        $to = new \DateTime();
        $from = clone $to;
        $from->sub(new \DateInterval('P2Y'));

        /** @var ChartPresenter $chartPresenter */
        $chartPresenter = $this->get('app.chart.historical.chart_presenter');
        $chartPresenter->init($from, $to, $user->getPortfolio());

        return $this->render(
            'portfolio/chart.html.twig',
            [
                'presenter' => $chartPresenter,
            ]
        );
    }
}
