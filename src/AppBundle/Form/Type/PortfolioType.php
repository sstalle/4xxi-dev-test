<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Portfolio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PortfolioType extends AbstractType
{
    const TYPE_NAME = 'app_portfolio';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'shares',
            'collection',
            [
                'type' => PortfolioShareType::TYPE_NAME,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'options' => [
                    'validation_groups' => $options['validation_groups'],
                ],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => Portfolio::class,
            'validation_groups' => ['portfolio'],
            'cascade_validation' => true,
            'method' => 'POST',
        ]);
    }

    public function getName()
    {
        return static::TYPE_NAME;
    }
}
