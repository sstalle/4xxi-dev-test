<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\PortfolioShare;
use AppBundle\Entity\Share;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PortfolioShareType extends AbstractType
{
    const TYPE_NAME = 'app_portfolio_share';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'share',
            'entity',
            [
                'class' => Share::class,
                'choice_label' => 'code',
                'label' => 'Акция',
            ]
        )->add(
            'count',
            'integer',
            [
                'scale' => 0,
                'label' => 'Количество',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => PortfolioShare::class,
            'validation_groups' => ['portfolio_share'],
            'method' => 'POST',
        ]);
    }

    public function getName()
    {
        return static::TYPE_NAME;
    }
}
