<?php

$finder = Symfony\CS\Finder\DefaultFinder::create()
    ->in(__DIR__)
    ->exclude([
        'vendor',
        'bin',
        'web',
        'app/cache',
        'app/logs',
    ]);

return Symfony\CS\Config\Config::create()
    ->finder($finder)
;